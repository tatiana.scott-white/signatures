# Architecture Decision Record: Adopt googleapis/nodejs-dlp Interfaces

[![Created on][octicon-calendar]][adr-0003] <time datetime="2019-03-20">2019-03-20</time>

## Status

[![adr: accepted][adr-accepted-badge]][adr-0003]

## Context

Google offers a Data Loss Prevention (DLP) API as part of its Cloud Platform services. Google Cloud DLP <q cite="https://github.com/googleapis/nodejs-dlp#readme">provides programmatic access to a powerful detection engine for personally identifiable information and other privacy-sensitive data in unstructured data streams.</q> [^1]

Not surprisingly, Google Cloud DLP also provides excellent documentation:

- [Data Loss Prevention (DLP) API Node.js Client API Reference ![Offsite link][octicon-link-external]][dlp-nodejs-client-api-ref]
- [github.com/googleapis/nodejs-dlp ![Offsite link][octicon-link-external]][nodejs-dlp-github-readme]
- [Data Loss Prevention (DLP) API Documentation ![Offsite link][octicon-link-external]][dlp-api-docs]
- [Client Libraries Explained ![Offsite link][octicon-link-external]][client-libs-url]

The client API's interface is simple without sacrificing semantics:

```js
// Imports the Google Cloud Data Loss Prevention library
const DLP = require('@google-cloud/dlp')

// Instantiates a client
const dlp = new DLP.DlpServiceClient()

// The string to inspect
const specimen = 'Robert Frost'

// The project ID to run the API call under
const projectId = process.env.GCLOUD_PROJECT

// The minimum likelihood required before returning a match
const minLikelihood = 'LIKELIHOOD_UNSPECIFIED'

// The maximum number of findings to report (0 = server maximum)
const maxFindings = 0

// The infoTypes of information to match
const infoTypes = [{name: 'PERSON_NAME'}, {name: 'US_STATE'}]

// Whether to include the matching string
const includeQuote = true

// Construct item to inspect
const item = {value: specimen}

// Construct request
const request = {
  parent: dlp.projectPath(projectId),
  inspectConfig: {
    infoTypes: infoTypes,
    minLikelihood: minLikelihood,
    limits: {
      maxFindingsPerRequest: maxFindings,
    },
    includeQuote,
  },
  item
}

// Run request
dlp
  .inspectContent(request)
  .then(response => {
    const findings = response[0].result.findings;
    if (findings.length > 0) {
      console.log(`Findings:`)
      findings.forEach(finding => {
        if (includeQuote) {
          console.log(`\tQuote: ${finding.quote}`)
        }
        console.log(`\tInfo type: ${finding.infoType.name}`)
        console.log(`\tLikelihood: ${finding.likelihood}`)
      });
    } else {
      console.log(`No findings.`)
    }
  })
  .catch(err => {
    console.error(`Error in inspectString: ${err.message || err}`)
  })
```

Finally, the googleapis/nodejs-dlp client is free open-source software, and distributed under the permissive [Apache-2.0 license ![Offsite link][octicon-link-external]][spdx-apache-2.0-license].

## Decision

1.   We will adopt the googleapis/nodejs-dlp as an _idiomatic_ interface for our data-leakage detection and prevention modules.

1.   We will reverse-engineer features based on the behaviors implied by the interface, e.g., provide data-leakage detection findings based on statistical probability and confidence (expressed by [`Likelihood` ![Offsite link][octicon-link-external]][nodejs-dlp-likelihood-doc]).

1.  We will use the [Gang of Four Strategy Pattern ![View GoF Strategy Pattern on refactoring.guru][octicon-link-external]](https://refactoring.guru/design-patterns/strategy) to implement data leakage detection whenever dealing with probabilities in order to maintain a common interface while experimenting with optimal detection algorithms.

## Consequences

We will follow a well-tested and semantically rich suite of interfaces for detecting and preventing data leakage.

## References

[^1]: _googleapis/nodejs-dlp_. (2019). _GitHub_. Retrieved 6 March 2019, from <https://github.com/googleapis/nodejs-dlp#readme>

<!-- Do not remove this line or anything under it. -->

[googleapis-nodejs-dlp-readme]: https://github.com/googleapis/nodejs-dlp#readme

[client-libs-url]: https://cloud.google.com/apis/docs/client-libraries-explained

[dlp-api-docs]: https://cloud.google.com/dlp/docs/

[dlp-nodejs-client-api-ref]: https://cloud.google.com/nodejs/docs/reference/dlp/latest/

[nodejs-dlp-github-readme]: https://github.com/googleapis/nodejs-dlp#readme

<!-- Do not remove this line or anything under it. -->

[adr-0003]: docs/adr/adr-0003-architecture-decision-record-adopt-googleapis%2fnodejs-dlp-interfaces.md

[adr-accepted-badge]: https://flat.badgen.net/badge/ADR/accepted/44AD8E
[adr-proposed-badge]: https://flat.badgen.net/badge/ADR/proposed/AC900D
[adr-rejected-badge]: https://flat.badgen.net/badge/ADR/rejected/D9534F
[adr-deprecated-badge]: https://flat.badgen.net/badge/ADR/deprecated/7F8C8D

<!-- Octicon image definition list -->

[octicon-calendar]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg

[nodejs-dlp-likelihood-doc]: https://cloud.google.com/nodejs/docs/reference/dlp/0.10.x/google.privacy.dlp.v2#.Likelihood

[spdx-apache-2.0-license]: https://spdx.org/licenses/Apache-2.0.html

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg
