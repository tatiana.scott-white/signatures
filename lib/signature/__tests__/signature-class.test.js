/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
const RE2 = require('re2')
const nullSignature = require('../null-signature')
const { Signature } = require('..')

describe('Signature', () => {
  describe('defines the prototype for IT secret definitions and identification', () => {
    describe('#constructor', () => {
      const contructorParams = {
        "caption": "Shell command history file",
        "description": "",
        "part": "filename",
        "pattern": "^.?(bash_|zsh_|sh_|z)?history$",
        "type": "regex"
      }
      let signature = null
      beforeEach(() => {
        signature = new Signature(contructorParams)
      })

      afterEach(() => {
        signature = null
      })

      describe('@param {string} caption', () => {
        it('summarizes the data-leakage detection defintion', () => {
          expect(signature.caption).toBe(contructorParams.caption)
        })
      })
      describe('@param {string} description', () => {
        it('optionally distinguishes definitions from each other with more details', () => {
          expect(signature.description).toBe(contructorParams.description)
        })
      })
      describe('@param {string} part', () => {
        describe('captures one of four valid file system objects to inspect', () => {
          it('"contents", (the data within a file)', () => {
            expect(signature.part).not.toBe('contents')
          })
          it(`"extension", (the group of letters following the period in a file name,
               indicating the format of a file.`, () => {
            signature.part = 'extension'
            expect(signature.part).toBe('extension')
          })
          it('"filename", (an identifier unique within any given directory)', () => {
            signature.part = 'filename'
            expect(signature.part).toBe('filename')
          })
          it('"path", (representation of a directory structure)', () => {
            signature.part = 'path'
            expect(signature.part).toBe('path')
          })
        })
      })
      describe('@param {string} pattern', () => {
        it('executable definition that matches a specific type of data leakage', () => {
          expect(signature.pattern).toBeInstanceOf(RE2)
          // const params = Object.assign({}, contructorParams)
          signature = new Signature()
        })
      })
      describe('@param {string} type', () => {
        describe('declares how to match a "pattern" with the value of a "part"', () => {
          it('"match" (a strict string equivalency--data type and value--check)', () => {
            expect(signature.type).toBe(contructorParams.type)
          })
        })
      })

      describe('when given no parameters', () => {
        it('creates a "nullSignature" object', () => {
          signature = new Signature()
          expect(signature).toEqual(nullSignature)
        })
      })
    })
    describe('#match(value)', () => {
      it('matches extension values', () => {
        signature = new Signature({
          part: 'extension',
          pattern: '.pem',
          type: 'MATCH'
        })
        let filePath = '/path/to/a/super-duper/effin/secret.pem'
        expect(signature.match(filePath)).toContain(filePath)
        filePath += 'bock'
        expect(signature.match(filePath)).toBeNull()
      })
      it('matches filename values', () => {
        signature = new Signature({
          "caption": "Environment configuration file",
          "description": "",
          "part": "filename",
          "pattern": ".env",
          "type": "match"
        })
        let filePath = '/path/to/repo-root/.env'
        expect(signature.match(filePath)).toContain(filePath)
        filePath += '.schema'
        expect(signature.match(filePath)).toBeNull()
      })
      it('matches path values', () => {
        signature = new Signature({
          "caption": "Pidgin chat client account configuration file",
          "description": "",
          "part": "path",
          "pattern": ".purple/accounts.xml",
          "type": "match"
        })
        let filePath = '/p/a/t/h/.purple/accounts.xml'
        expect(signature.match(filePath)).toContain(filePath)
        filePath += '/l/o/w/e/r'
        expect(signature.match(filePath)).toContain(filePath)
        filePath = '/your/name/here'
        expect(signature.match(filePath)).toBeNull()
      })
      it('matches string values with RE2 or Regexps', () => {
        signature = new Signature({
          "part": "filename",
          "pattern": "^.?npmrc$",
          "type": "regex"
        })
        expect(signature.match('npmrc')).toEqual(['npmrc'])
        expect(signature.match('.npmrc')).toEqual(['.npmrc'])
        expect(signature.match('.rpmrc')).toBeNull()
      })
    })
    describe('#toJson', () => {
      it('serializes a signature as JSON', () => {
        const literal = JSON.parse('{"caption":"Contains word: secret","description":null,"part":"filename","pattern":"secret","type":"regex"}')
        const signature = new Signature(literal)
        expect(signature.pattern).toBeInstanceOf(RE2)
        expect(signature.toJson()).toContain('"pattern":"/secret/gimu"')
      })
    })
    describe('#toString', () => {
      it('serializes the signature as JSON', () => {
        const expectedJson =
          '{"caption":"1Password password manager database file","description":null,"hash":"a6e0a13","name":"1-password-password-manager-database-file","part":"extension","pattern":"agilekeychain","type":"match"}'
        const signature = new Signature({
          'caption': '1Password password manager database file',
          'description': null,
          'part': 'extension',
          'pattern': 'agilekeychain',
          'type': 'match'
        })
        expect(signature.toString()).toBe(expectedJson)
      })
    })
    describe('#valueOf', () => {
      it('is overridden to return @pattern as a String', () => {
        const sshKeySignature = new Signature({
            "caption": "Private SSH key",
            "description": "",
            "part": "filename",
            "pattern": "^.*_dsa$",
            "type": "regex"
        })
        expect(sshKeySignature.valueOf()).toBe('/^.*_dsa$/gimu')

        const logFileSignature = new Signature({
          "caption": "Log file",
          "description": "Log files can contain secret HTTP endpoints, session IDs, API keys and other goodies",
          "part": "extension",
          "pattern": ".log",
          "type": "match"
        })

        expect(logFileSignature.valueOf()).toBe('.log')
      })
    })
    describe('RE2 throws errors when', () => {
      const signature = {
        pattern: '?!Perl look-ahead',
        type: 'regex'
      }
      expect(() => {
        signature.match('test')
      }).toThrow()
    })
  })
})
