const toRe2 = require('../to-re2')

describe('toRe2', () => {
  const replacementText = '<REDACTED>'
  describe('when given a string representation of a regex', () => {
    it('creates an RE2 instance', () => {
      const pattern = '/AKIA[0-9A-Z]{16}/gimu'
      const regex = toRe2({
        pattern,
        type: 'regex'
      })
      expect(regex.toString()).toBe(pattern)
      expect('AKIA0000000000000000'.replace(regex, replacementText)).toBe(
        replacementText
      )
    })
  })

  describe('when given a regular expression', () => {
    it('creates an RE2 instance', () => {
      const pattern = /AKIA[0-9A-Z]{16}/gimu
      const regex = toRe2({
        pattern,
        type: 'regex'
      })
      expect(regex.toString()).toBe('/AKIA[0-9A-Z]{16}/gimu')
      expect('AKIA0000000000000000'.replace(regex, replacementText)).toBe(
        replacementText
      )
    })
  })
})
