const Signature = require('./signature')
const nullSignature = require('./null-signature')
const validSignatureParts = require('./valid-signature-parts')
const validSignatureTypes = require('./valid-signature-types')

const signature = {
  Signature,

  /**
   * Create a new Signature instance.
   *
   * @param {object} [params=nullSignature] - The Signature instance property values to set.
   * @memberof dataLossSignatures.signature
   * @returns {Signature}
   */

  factory (params = nullSignature) {
    return new Signature(params)
  },

  nullSignature,

  /**
   * @description Convert a JSON string or object-literal array into Signature
   * instances.
   * @author $(git config user.name)
   * @date 2019-02-22
   * @param {string|Array.<object>} data - String or Object array.
   * @returns An array of Signature instances.
   */

  toArray (data) {
    let signatureList = data
    if (typeof signatureList === 'string') {
      signatureList = JSON.parse(data)
    }
    return signatureList.map((attrs) => new Signature(attrs))
  },

  validParts: validSignatureParts,

  validTypes: validSignatureTypes
}

module.exports = signature
