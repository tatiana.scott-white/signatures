const assignHashPropertyTo = require('./mixins/assign-hash-property-to')
const validSignatureParts = require('./valid-signature-parts')
const validSignatureTypes = require('./valid-signature-types')

/**
 * @memberof dataLossSignatures.signature
 * @inner
 */

const nullSignature = {
  caption: null,
  description: null,
  name: '',
  part: validSignatureParts.UNKNOWN,
  pattern: null,
  type: validSignatureTypes.UNKNOWN
}

assignHashPropertyTo(nullSignature)

module.exports = nullSignature
