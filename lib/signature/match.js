const { basename, extname } = require('path')
const { EXTENSION, FILENAME, PATH } = require('./valid-signature-parts')
const { REGEX } = require('./valid-signature-types')

const matchExtensions = (value, { pattern }) => {
  const ext = extname(value)
  if (pattern === ext) {
    return [value]
  }
  return null
}

const matchFileNames = (value, { pattern }) => {
  const fileName = basename(value)
  if (pattern === fileName) {
    return [value]
  }
  return null
}

const matchPaths = (value, { pattern }) => {
  if (value.includes(pattern)) {
    return [value]
  }
  return null
}

const matchFsMap = new Map([
  [
    EXTENSION,
    matchExtensions
  ],
  [
    FILENAME,
    matchFileNames
  ],
  [
    PATH,
    matchPaths
  ]
])

const match = (value, signatureRef) => {
  if (signatureRef.type === REGEX) {
    return signatureRef.pattern.match(value)
  }
  return matchFsMap.get(signatureRef.part)(value, signatureRef)
}

module.exports = match
