const kebabCase = require('lodash.kebabcase')

const assignNamePropertyTo = (signature) =>
  Object.defineProperty(signature, 'name', {
    configurable: false,
    enumerable: true,
    value: kebabCase(signature.caption),
    writable: false
  })

module.exports = assignNamePropertyTo
