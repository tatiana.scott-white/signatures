const abbrev = require('./abbrev')
const { hash } = require('node-object-hash')({
  sort: true
})

const assignHashPropertyTo = (signature) => {
  const { caption, description, name, part, pattern, type } = signature
  Object.defineProperty(signature, 'hash', {
    configurable: false,
    enumerable: true,
    value: abbrev(
      hash({
        caption,
        description,
        name,
        part,
        pattern,
        type
      })
    ),
    writable: false
  })
}

module.exports = assignHashPropertyTo
