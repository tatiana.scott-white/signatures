const START = 0
const HASH_LENGTH = 7
const defaultOptions = {
  length: HASH_LENGTH,
  start: START
}

const abbrev = (hash, options = defaultOptions) => {
  const { start, length } = options
  return hash.substring(start, length)
}

module.exports = abbrev
