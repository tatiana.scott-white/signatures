const { validParts } = require('../../../signature')

const { EXTENSION, FILENAME, PATH } = validParts

const byFsParts = (signature) => [
  EXTENSION,
  FILENAME,
  PATH
].includes(signature.part)

module.exports = byFsParts
