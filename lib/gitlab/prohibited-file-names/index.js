const debug = require('debug')('prohibited-file-names')

const byFsParts = require('./filter/by-fs-parts')
const fsExtra = require('fs-extra')
const signature = require('../../signature')
const signatures = require('../../../signatures.json')
const template = require('lodash.template')
const toExpression = require('./to-expression')

const fileBlacklistTemplate = `
# List of regular expressions to prevent secrets
# being pushed to repository.
# This list is checked only if project.push_rule.prevent_secrets is true
# Any changes to this file should be documented in: doc/push_rules/push_rules.md

<% signatures.forEach((signature) => { %>
###
# <%= signature.caption %>
# <%= signature.description %>
###

- <%= signature.pattern %>

<% }) %>

`
const prohibitedFileNames = {
  async save (destination = './files_blacklist.yml') {
    try {
      const yaml = this.toFilesBlacklistYaml()
      await fsExtra.outputFile(destination, yaml)
      return true
    } catch (err) {
      return false
    }
  },

  'signatures': signature.toArray(signatures),

  toFilesBlacklistYaml () {
    const compiled = template(fileBlacklistTemplate)
    return compiled({
      signatures
    })
  },

  toString () {
    const concatenatedExpression = this.signatures
      .filter((sign) => byFsParts(sign))
      .map((sign) =>
        toExpression(sign)
          .replace('/', '')
          .replace('/gimu', '')
          .replace('\\/', '/')
      )
      .sort()
      .join('|')
    debug(`(${concatenatedExpression}), (${this.signatures.length})`)
    return `(${concatenatedExpression})`
  }
}

module.exports = prohibitedFileNames
