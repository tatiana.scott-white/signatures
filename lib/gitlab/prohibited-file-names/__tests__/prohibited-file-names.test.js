const fsExtra = require('fs-extra')
const prohibitedFileNames = require('../index.js');

describe('prohibitedFileNames', () => {

  describe('.save', () => {
    describe('(over)writes a new "files_blacklist.yml" file to disc', () => {
      it('saves a file to disc, creating new directories if needed', async done => {
        fsExtra.outputFile = jest.fn().mockResolvedValueOnce(true)
        const saved = await prohibitedFileNames.save()
        expect(saved).toBe(true)
        done()
      })
      it('returns "false" if the file cannot be saved', async done => {
        fsExtra.outputFile = jest.fn().mockRejectedValueOnce(false)
        const saved = await prohibitedFileNames.save()
        expect(saved).toBe(false)
        done()
      })
    })
  })
  describe('.toFilesBlacklistYaml', () => {
    it('creates the file_blacklist.yml body', () => {
      const yamlBody = prohibitedFileNames.toFilesBlacklistYaml()
      expect(yamlBody).toBeDefined()
    })
  })

  describe('.toString', () => {
    it('generates a concatenated regular', () => {
      expect(() => {
        prohibitedFileNames.toString()
      }).not.toThrow()
    })

  })

});
