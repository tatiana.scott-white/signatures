const eslint = require('eslint')
var eslintFindRules = require('eslint-find-rules')
var ruleFinder = eslintFindRules('./.eslintrc.yaml')

// default to the `main` in your `package.json`
// var ruleFinder = eslintFindRules()

// get all the current, plugin, available and unused rules
// without referring the extended files or documentation

// ruleFinder.getCurrentRules()

let detailedRules = ruleFinder.getCurrentRulesDetailed()
console.log(detailedRules)
console.log(JSON.stringify(detailedRules, null, '  '))
console.log(ruleFinder)

// ruleFinder.getPluginRules()

// ruleFinder.getAllAvailableRules()

// ruleFinder.getUnusedRules()

// ruleFinder.getDeprecatedRules()
const configFile = './.eslintrc.yaml'
const cliEngine = new eslint.CLIEngine({
  // Ignore any config applicable depending on the location on the filesystem
  useEslintrc: false,
  // Point to the particular config
  configFile
})
console.log(cliEngine.getConfigForFile())
