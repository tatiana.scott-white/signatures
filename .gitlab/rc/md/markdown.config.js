/* eslint global-require: "off" */
/* eslint node/no-unpublished-require: "off" */
const markdownMagic = require('markdown-magic')
const code = require('markdown-magic/lib/transforms/code')
const toc = require('markdown-magic/lib/transforms/toc')
const markdownMagicDependencyTable = require('markdown-magic-dependency-table')
const markdownMagicDirectoryTree = require('markdown-magic-directory-tree')
const markdownMagicPackageScripts = require('markdown-magic-package-scripts')
const path = require('path')

const wrapWithNewlines = (fn, contents, options, callback) => `\n${fn(contents, options, callback)}\n`

const config = {
  DEBUG: false,
  transforms: {
    CODE(contents, options, config) {
      const output = wrapWithNewlines(code, contents, options, config)
      return output.replace(/^.*The below code snippet is automatically added from .*$/, '\n')
    },
    DEPENDENCYTABLE(contents, options, callback) {
      const dependencyTableMarkdown = markdownMagicDependencyTable(contents, options, callback)
        .trim()
        .replace(/^\s{1}/gimu, '')
      return `\n${dependencyTableMarkdown}\n`
    },
    DIRTREE(contents, options, callback) {
      const opts = Object.assign(options, {
        ignore: ['.DS_Store', '.env', '.git', '.gitkeep', '.scannerwork', 'api', 'coverage', 'eslint-report.json', 'img', 'jest-junit.xml', 'node_modules', 'scripts', 'sonar-test-report.xml']
      })

      const output = wrapWithNewlines(markdownMagicDirectoryTree, contents, opts, callback)

      return output.replace('@data-leakage-protection/signatures/', '@data-leakage-protection/signatures/\n├─┬ .git/**')
    },
    SCRIPTS(contents, options, callback) {
      return wrapWithNewlines(markdownMagicPackageScripts, contents, options, callback)
    },
    TOC(contents, options, config) {
      return wrapWithNewlines(toc, contents, options, config)
    }
  }
}

const main = () => {
  const paths = ['*.md', 'docs/*.md', 'docs/**/*.md']

  paths.forEach(filePath => {
    const markdownPath = path.join(filePath)
    markdownMagic(markdownPath, config)
  })
}

main()
