# Architecture Decision Record: REPLACE_THIS_PLACEHOLDER_WITH_YOUR_SUBJECT

<!-- 

Step 1.  "Getting started"

    ⏸ PAUSE: Read the following "TIP" and "INSTRUCTIONS" before you begin.

    1.1. 💡 TIP: Select "Preview Markdown" to review your ADR. 

    1.2. 🎓 INSTRUCTIONS: If you get stuck, view our "Architecture Decisions (AD) 
            Guide:"
        
https://gitlab.com/data-leakage-protection/signatures/wikis/Governance/Architecture-Decisions

    /🎓🎓🎓🎓🎓🎓-->

<!-- Step 2. "Decided on date"

    🎓 INSTRUCTIONS: A Trusted Committer will take care of the date info on
                     line 23; you can leave it alone.
    
-->

![Decided on date][octicon-calendar] <time datetime="YYYY-MM-DD">YYYY-MM-DD</time>

## Status

<!-- Step 3. "Status"

    🎓 INSTRUCTIONS: Architecture Decision Records always begin in a
                     "proposed" state. A Trusted Committer will modify
                     line 35 after the community votes on it.
    
-->

![adr: proposed][adr-proposed-badge]

<!-- Step 3. "Status continued"

  🚫 Please leave the commented badges on lines 47-51 alone: they're here for
     convenience once your ADR have been voted on.

  👮‍♀️ TRUSTED COMMITTERS: See the Wiki for details:
    
      https://gitlab.com/data-leakage-protection/signatures/wikis/Style-Guides/Images

  ![adr: accepted][adr-accepted-badge]

  ![adr: rejected][adr-rejected-badge]

  ![adr: deprecated][adr-deprecated-badge]

--->

## Context

Add motivations and context....

## Decision

State your decision here....

## Consequences

State the consequences of this decision....

## References

Remove this section if you don't need it. Otherwise, [use APA citations][citations].

<!-- ⛔️ Do not remove this line or anything under it ⛔️ -->

---

[![Need help? See the "Architecture Decision (AD) Guide"][octicon-question] _Need **help**? See the "Architecture Decision (AD) Guide."_][architecture-decisions-guide]

---

[adr-accepted-badge]: https://flat.badgen.net/badge/ADR/accepted/44AD8E

[adr-proposed-badge]: https://flat.badgen.net/badge/ADR/proposed/AC900D

[adr-rejected-badge]: https://flat.badgen.net/badge/ADR/rejected/D9534F

[adr-deprecated-badge]: https://flat.badgen.net/badge/ADR/deprecated/7F8C8D

[architecture-decisions-guide]: https://gitlab.com/data-leakage-protection/signatures/wikis/Governance/Architecture-Decisions

[citations]: https://gitlab.com/data-leakage-protection/signatures/wikis/Style-Guides/Citations-(APA) "Go to our Wiki page for Citation Guidelines."

[octicon-calendar]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg

[octicon-question]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg
